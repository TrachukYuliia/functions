const getSum = (str1, str2) => {
  if(typeof(str1) != "string" || typeof(str2) != "string" )
     return false;
  else 
     {
      if(str1 == ' ')
        str1 = 0;
      else if(str2 == ' ')
        str2 = 0;
      for(let i = 0; i < str1.length; i++)
      {
         if(str1[i].match(/[a-z]/i))
          return false;
      }
      for(let j = 0; j < str2.length; j++)
      {
         if(str2[j].match(/[a-z]/i))
          return false;
      }
    return String(Number(str1) + Number(str2));
     }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var counterPost = 0;
  var counterComments = 0;
  for(let value of listOfPosts)
  {
       for (var key in value)
     {
           if(value[key] == authorName)
             {  
              counterPost ++;
             }
            if(key == 'comments')
               for(let newValue of value[key])
                    for (var key2 in newValue)
                        if(newValue[key2] == authorName)
                          counterComments ++;

     }
  }
  return 'Post:' + counterPost + ',comments:' + counterComments;
};

const tickets=(people)=> {
  var counter = 0;
  var flag = true;
  for (var i = 0; i< people.length; i++)
   {
      if( people[i]  == 25)
       {
           ++counter;
       }
    else
     { 
         if(counter - people[i]/25 + 1  >= 0)
      {
          counter = counter - (people[i]/25 - 1);
          counter += people[i]/25
         
      }
         else
            flag = false
     }
    }
  if(flag)
    return "YES"
  else 
    return "NO";
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
